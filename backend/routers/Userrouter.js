import express from 'express';
import getAllUsers , { deleteUser, login, register, updateUser } from '../controlletrs/Usercon.js';

const userRouter = express.Router();

// userRouter.get('/', (req, res) => {
//     res.send('Hello World!');
// });

userRouter.get('/',getAllUsers);
userRouter.post('/register', register);
userRouter.put('/:Email', updateUser);
userRouter.delete('/:id', deleteUser);

userRouter.post('/login', login)    

export default userRouter;