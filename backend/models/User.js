import mongoose from 'mongoose';
// 1. Create a schema
const Schema = mongoose.Schema;

const userSchema = new Schema({
    Name: {
        type:String,
        required:true
    },
    Gender:{
        type:String,
        require:true
    },
    Age:{
        type:String,
        require:true
    },
    MobileNumber:{
        type:String,
        unique:true,
        minLength:10,
        require:true
    },
    Email: {
        type:String,
        required:true,
        unique: true
    },
    password: {
        type:String,
        required:true,
        unique: true,
        minLength: 4,
    },
    // ConfromPassword:{
    //     type:String,
    //     require:true,
    //     unique:true,
    //     minLength:6
    // }
})

// 2. Export the model
const  User = mongoose.model('users', userSchema);
export default User;