import {createStore} from 'redux'
import reducer from './Reducers/Reducer'
import {composeWithDevTools} from 'redux-devtools-extension';



const devtool = composeWithDevTools();
export const store = createStore(reducer,devtool);