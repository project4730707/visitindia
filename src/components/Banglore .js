import React from 'react'
import './TripStyle.css'
import { Tripdata } from './Tripdata'
import Banglore1 from '../assets/Banglore/Banglore palace/img1.jpg'
import Banglore2 from '../assets/Banglore/Cubbon park/img1.jpg'
import Banglore3 from '../assets/Banglore/Lalbagh/img1.jpg'
import Banglore4 from '../assets/Banglore/National park/img1.jpg'

export const Banglore  = () => {
  return (
<div className='trip'>
      <div className='tripcard'>
        <Tripdata 
         image={Banglore1}
         Heading="Banglore palace"
         text="the central attraction in Bangalore, the palace was built in the year 1878.  A mixture of Tudor and Scottish Gothic architecture have resulted into the grand palace that we see today."
         time="Best Timing to Visit : 10:00 AM - 5:30 PM"
         dis="Nearest Stations:Bengaluru Cantonment is the nearest railway station (3 kms)."
         />

<Tripdata 
         image={Banglore3}
         Heading="Lalbagh"
         text="A haven for all nature lovers, Lal Bagh covers an area 240 acres in the heart of the city and has nearly 1,854 species of plants. It was commissioned by Hyder Ali in 1760 and completed by his son Tipu Sultan."
         time="Best Timing to Visit: 6:00 AM - 7:00 PM"
         dis="Nearest Stations:distance of 6 km from Bangalore City Railway Station."
         />

<Tripdata 
         image={Banglore2}
         Heading="Cubbon park"
         text="Cubbon Park in the city of Bangalore is a major sightseeing attraction rich in green foliage. It is a green belt region of the city and is an ideal place for nature lovers and those seeking a calm atmosphere."
         time="Best Timing to Visit: Tuesday - 6:00 AM - 6:00 PM Closed on Mondays and second Tuesdays of Every month.Strictly for morning walkers: 6:00 AM - 8:00 AM"
         dis="Nearest Stations:Cubbon Park and Vidhana Soudha metro stations, that are right next to two different entrances of Cubbon Park."
         />
<Tripdata 
         image={Banglore4}
         Heading="National park"
         text="the Bannerghatta National Park is a sanctuary for a large variety of flora and fauna. Spread over a massive area of around 104.27 sq. km, this national park was established in the year 1971."
         time="Best Timing to Visit: Timings of Bannerghatta Zoo : Butterfly park and boating: 9:30 AM - 5:00 PM,
                                      Grand Safari: 10:00 AM - 4:30 PM,Closed on Tuesdays"
         dis="Nearest Stations:Bilekahalli is 86 meters away, 2 min walk."
         />
         </div>
    </div>
  )
}
