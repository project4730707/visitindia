import "./TripStyle.css"
import Agra1 from '../assets/Agra/Agra fort/img1.jpg'
import Alleppey from '../assets/Alleppey/Houseboats/img1.webp'
import Banglore from '../assets/Banglore/Lalbagh/img1.jpg'
import React from 'react'
import { Tripdata } from "./Tripdata"


export const Trip = () => {
  return (
    <div className="trip">
        <h1>Recent Trip</h1>
        <p>You can discover unique destination using Google Maps. </p>
         <div className="tripcard">
         <Tripdata 
         image={Agra1}
         heading="Agra"
         text="Agra is a city on the banks of the Yamuna river in the Indian state of Uttar Pradesh, about 230 kilometres south-east of the national capital Delhi and 330 km west of the state capital Lucknow."
         url="login"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Alleppey}
         heading="Alleppey"
         text="Alappuzha (or Alleppey) is a city on the Laccadive Sea in the southern Indian state of Kerala. It's best known for houseboat cruises along the rustic Kerala backwaters, a network of tranquil canals and lagoons. "
         url="login"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Banglore}
         heading="Banglore"
         text="Bengaluru (also called Bangalore) is the capital of India's southern Karnataka state. The center of India's high-tech industry, the city is also known for its parks and nightlife. By Cubbon Park, Vidhana Soudha is a Neo-Dravidian legislative building. "
         url="login"
         btnText="visit"
         btnClass="cbtn"
         />
         </div>
    </div>
  )
}
