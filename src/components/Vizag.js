import React from 'react'

export default function Vizag() {
  return (
    <div>
      <div>
        <h2>Borra caves</h2>
        <p>Located on the east coast of India, the Borra Caves are situated in the Ananthagiri hills of the Araku Valley in Visakhapatnam district.</p>
        <h6>Timings:<span>Monday to Sunday: 10:00 AM - 5:00 PM</span></h6>
        <h6>Nearest Stations:<span>The distance between Borra Caves and Araku Valley is 36 km. The road distance is 35.8 km.</span></h6>
      </div>
      <div>
        <h2>Kailasagiri</h2>
        <p>Located in the scenic locales of Vizag, Kailash Giri is a beautiful hilltop park perched at a height of 360 ft. Sprawling over 100 acres of lush green land area, the park is perched on a hilltop and it offers scenic panoramic vistas of the surroundings and the beach below. </p>
        <h6>Timings:<span>10 AM - 8 PM</span></h6>
        <h6>Nearest Stations:<span>Kailasagiri is located around 10 km from the Visakhapatnam Railway Station and around 8 km from Visakhapatnam Dwaraka Bus Station</span></h6>
      </div>
      <div>
        <h2>RK beach</h2>
        <p> One of the most popular beaches of Vishakapatnam, RK beach is a beach for to spend some relaxed time, enjoy strolls across the beach, sunbath and more.</p>
        <h6>Timings:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span>Ramakrishna Beach is located at a distance of 5 km from the Visakhapatnam Railway Station.</span></h6>
      </div>
      <div>
        <h2>Submarine Museum</h2>
        <p>Submarine Museum is located in Visakhapatnam in the south-eastern state of India. The museum is inside an actual submarine, INS Kurusura, which was India's 5th submarine.</p>
        <h6>Timings:<span>2:00 PM - 8:30 PM (Closed on Monday)</span></h6>
        <h6>Nearest Stations:<span>The nearest railway station is Visakhapatnam which is 4.8km from the Museum.</span></h6>
      </div>
    </div>
  )
}
