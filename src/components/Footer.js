import React from 'react'
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaXTwitter } from "react-icons/fa6";
import { FaLinkedin } from "react-icons/fa6";


import "./FooterStyle.css"

export const Footer = () => {
  return (
    <div className='footer'>
        <div className='top'>
          <div>
            <h1>Visit India</h1>
            <p>Choose your favourite destionation.</p>
          </div>
          <div>
          <a href='/'><FaFacebook /></a>
            <a href='/'><FaInstagram /></a>
            <a href='/'><FaXTwitter /></a>
            <a href='/'><FaLinkedin /></a>
          </div>
        </div>
        <div className='bottom'>
          <div>
            <h4>Project</h4>
            <a href='/'>Changlog</a>
            <a href='/'>Status</a>
            <a href='/'>License</a>
            <a href='/'>All Versions</a>
          </div>
          <div>
            <h4>Explore India</h4>
            <a href='/'>Historical Places</a>
            <a href='/'>Trip Places</a>
            <a href='/'>Destination</a>
            <a href='/'>AboutUs</a>
          </div>
        </div>
    </div>
  )
}
