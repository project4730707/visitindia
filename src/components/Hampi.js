import React from 'react'

export default function Hampi() {
  return (
    <div>
      <div>
        <h2>Virupaksha temple</h2>
        <p>The Virupaksha temple (or Prasanna Virupaksha temple) is located on the banks of the Tungabhadra river at Hampi. Built during the 7th century, the beautiful architecture and history of the temple have made it a UNESCO World Heritage site.</p>
        <h6>Timings:<span>9:00 AM - 1:00 PM, 5:00 PM - 9:00 PM</span></h6>
        <h6>Neatrest Stations:<span>The Hospet Junction Railway Station is situated at a distance of around 13 km from Hampi. </span></h6>
      </div>
      <div>
        <h2>Lotus Palace</h2>
        <p>The Lotus Palace in Hampi is one of the most iconic landmarks in the town. Named so for the way the structure looks like a lotus in bloom, this palace was the designated area for the royal women of the Vijayanagara Empire and is found within the Zenana Enclosure.</p>
        <h6>Timings:<span>Open on Weekdays from 8:00 am to 6:00 pm</span></h6>
        <h6>Nearest Stations:<span>Hosapete is the nearest railway station </span></h6>
      </div>
      <div>
        <h2>Vithala temple</h2>
        <p>The most impressive structure in Hampi, the Vithala Temple dates back to the 16th century and is a truly splendid example of rich architecture. </p>
        <h6>Timings: <span>8:30 am to 6:00pm</span></h6>
        <h6>Nearest Stations:<span>Nearest Railway Station :Hospet Junction Railway Station.Nearest Bus Station : KSRTC Bus Station, Hospet.
</span></h6>
      </div>
    </div>
  )
}
