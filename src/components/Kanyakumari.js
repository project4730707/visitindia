import React from 'react'

export const Kanyakumari = () => {
  return (
    <div>
       <div>
        <h2>Beach</h2>
        <p>Located in the southernmost part of India, Kanyakumari beach with its beautiful hue-changing beaches, the confluence of three water bodies: Bay of Bengal, Indian Ocean, and the Arabian Sea.</p>
        <h6>Timing:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span>The Kanyakumari rail station is located in the heart of Kanyakumari and is only 1 kilometre (0.62 mi) away from Kanyakumari beach.</span></h6>
      </div>
      <div>
        <h2>Padhmanabhapuram palace</h2>
        <p>Padmanabhapuram Palace in Tamil Nadu is one of the most exquisite palaces of India that symbolize the rich and diverse cultural heritage of the country that has been around for many centuries aptly.</p>
        <h6>Timings:<span> 9:00 AM- 5:00 PM</span></h6>
        <h6>Nearest Stations:<span>The nearest railway station to the palace is Nagarcoil railway station.</span></h6>
      </div>
      <div>
        <h2>Rock Memorial</h2>
        <p>The magnificent Vivekananda Rock Memorial is located on a small island off Kanyakumari. It has the picturesque Indian Ocean in its backdrop. </p>
        <h6>Timings:<span>: 7:00 AM till 5:00 PM</span></h6>
        <h6>Nearest Stations:<span>At a distance of 1.5 km from Kanyakumari Bus Station & 1.5 km from Kanyakumari Railway Station</span></h6>
      </div>
    </div>
  )
}
