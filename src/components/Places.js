import React from 'react'
import './TripStyle.css'
import { Tripdata } from './Tripdata'
import Agra1 from '../assets/Agra/Agra fort/img1.jpg'
import Alleppey from '../assets/Alleppey/Houseboats/img1.webp'
import Banglore from '../assets/Banglore/Lalbagh/img1.jpg'
import Chennai  from '../assets/Chennai/MGR film city/img1.jpg'
import Coimbatore from '../assets/Coimbatore/Kovai kondattam/img2.jpg'
import Delhi from '../assets/Delhi/Qutub minar/img2.jpg'
import Hyderabad from '../assets/Hyderabad/Charminar/img1.jpg'
import Jaipur from '../assets/Jaipur/Amber fort/img1.jpg'
import Ladakh from '../assets/Ladakh/Magnetic hill/img2.jpg'
import Jammu  from '../assets/Jammu/Temple/img2.jpg'
import Kanyakumri from '../assets/Kanyakumari/Rock memorial/img2.jpg'
import Mysore from '../assets/Mysore/Gardens/img2.jpg'
import Swiperslide from '../pages/Siwperslide'


export const Places = () => {
  return (
    
    <div className='trip'>
        <Swiperslide />
        <h1>Places You Visit</h1>
        <p>You can discover unique destination using Google Maps. </p>
        <div className='tripcard'>
        <Tripdata 
         image={Agra1}
         heading="Agra"
         text="Agra is a city on the banks of the Yamuna river in the Indian state of Uttar Pradesh, about 230 kilometres south-east of the national capital Delhi and 330 km west of the state capital Lucknow."
         url="agra"
         btnText="visit"
         btnClass="cbtn"
         />

         <Tripdata 
         image={Alleppey}
         heading="Alleppey"
         text="Alappuzha (or Alleppey) is a city on the Laccadive Sea in the southern Indian state of Kerala. It's best known for houseboat cruises along the rustic Kerala backwaters, a network of tranquil canals and lagoons. "
         url="alleppey"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Banglore}
         heading="Banglore"
         text="Bengaluru (also called Bangalore) is the capital of India's southern Karnataka state. The center of India's high-tech industry, the city is also known for its parks and nightlife. By Cubbon Park, Vidhana Soudha is a Neo-Dravidian legislative building. "
         url="banglore"
         btnText="visit"
         btnClass="cbtn"
         />

         </div>
         
         <div className='tripcard'>
        <Tripdata 
         image={Chennai}
         heading="Chennai"
         text="the MGR Film city is managed by the Government of Tamil Nadu in loving memory of MG Ramachandran who was not only a crowd-pleasing tamil actor but also a longtime CM of Tamil Nadu."
         url="chennai"
         btnText="visit"
         btnClass="cbtn"
         />

         <Tripdata 
         image={Coimbatore}
         heading="Coimbatore"
         text="Kovai Kondattam is an amusement park located about 2 kilometres away from Perur, in Coimbatore, Tamil Nadu. A fun-filled destination ideal for spending hours of unending entertainment with friends and family, Kodai Kondattam is an eco-friendly theme park.  "
         url="coimbatore"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Delhi}
         heading="Delhi"
         text="The All India War Memorial, popularly known as the India Gate, is located along the Rajpath in New delhi. This 42-meter tall historical structure was designed by Sir Edwin Lutyens and is one of the largest war memorials in the country. India Gate is also famous for hosting the Republic Day Parade every year. "
         url="delhi"
         btnText="visit"
         btnClass="cbtn"
         />

         </div>

         <div className='tripcard'>
        <Tripdata 
         image={Hyderabad}
         heading="Hyderabad"
         text="Situated in the Western part of the beautiful city of Hyderabad at a distance of approximately 9 km from Hussain Sagar Lake, Golconda Fort is one of the region's best-preserved monuments. "
         url="hyderabad"
         btnText="visit"
         btnClass="cbtn"
         />

         <Tripdata 
         image={Jaipur}
         heading="Jaipur"
         text=": Cradled on the top of a hill near Jaipur lies the Amer Fort, one of the most magnificent palaces in India. Also commonly known as the Amber Fort. "
         url="jaipur"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Ladakh}
         heading="Ladakh"
         text="Ladakh is a region administered by India as a union territory and constitutes an eastern portion of the larger Kashmir region that has been the subject of a dispute between India and Pakistan since 1947 and India and China since 1959.  "
         url="ladakh"
         btnText="visit"
         btnClass="cbtn"
         />

         </div>

         <div className='tripcard'>
         <Tripdata 
         image={Kanyakumri}
         heading="Kanyakumri"
         text="Kanyakumari is a coastal town in the state of Tamil Nadu on India's southern tip. Jutting into the Laccadive Sea, the town was known as Cape Comorin during British rule and is popular for watching sunrise and sunset over the ocean"
         url="kanyakumari"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Mysore}
         heading="Mysore"
         text="The Brindavan Gardens, spread over 60 acres, is located at a distance of 21 km away from Mysore. Built across the notable river of India, Cauvery, it took around five years to complete the project."
         url="mysore"
         btnText="visit"
         btnClass="cbtn"
         />

        <Tripdata 
         image={Jammu}
         heading="Jammu"
         text="The Raghunath Temple is a prominent temple and tourist spot that is located in Jammu, in the Indian state of Jammu and Kashmir. The temple is also one of the biggest complexes in entire northern India and is brimming with devotees all year round."
         url="jammu"
         btnText="visit"
         btnClass="cbtn"
         />

         </div>
    </div>
  )
}
