import React from 'react'

export const Jammu = () => {
  return (
    <div>
      <div>
        <h2>Bahu fort</h2>
        <p>The impeccable beauty and magnificence of the Bahu Fort surely stand to be a significant contributing factor to the popularity of Jammu. Located only 5 km from the central part of the city, the Bahu Fort stands tall and sturdy on the left bank of the Tawi River.</p>
        <h6>Timings:<span>: 5:00 AM - 10:00 PM</span></h6>
        <h6>Nearest Stations:<span>By Train. 4 Kms from Railway Station Jammu Tawi.By Road. 4.5 Kms from Bus Stand Jammu.
</span></h6>
      </div>
      <div>
        <h2>Vaishno Devi</h2>
        <p>Vaishno Devi is a temple town that's home to the famous Vaishno Devi Mandir. Located in Trikuta hills, 13 kms from Katra (in the union territory of Jammu and Kashmir); this renowned shrine allures millions of devotees from all over the world.  </p>
        <h6>Timings:<span>Throughout the year</span></h6>
        <h6>Nearest Stations:<span>Shri Mata Vaishno Devi Katra railway station (SVDK and Jammu Tawi railway station (JAT).</span></h6>
      </div>
      <div>
        <h2>Raghunath Temple</h2>
        <p>The Raghunath Temple is a prominent temple and tourist spot that is located in Jammu, in the Indian state of Jammu and Kashmir. The temple is also one of the biggest complexes in entire northern India and is brimming with devotees all year round. </p>
        <h6>Timings:<span>6:00 AM - 8:00 PM</span></h6>
        <h6>Nearest Stations:<span>Jammu Tawi, The temple is located at a distance of about approx 3 km from the railway station.</span></h6>
      </div>
    </div>
  )
}
