import React from 'react'

export const Mysore = () => {
  return (
    <div>
       <div>
        <h2>Brindavan gardens</h2>
        <p>The Brindavan Gardens, spread over 60 acres, is located at a distance of 21 km away from Mysore. Built across the notable river of India, Cauvery, it took around five years to complete the project.</p>
        <h6>Timings:<span>: Garden timings: 7:00 AM - 8:00 PM (Weekdays)
7:00 AM - 9:00 PM (Weekends) 
Musical Fountain Show: 6:30 PM - 8:00 PM (Weekdays)
6:30 PM - 9:00 PM (Weekends)
</span></h6>
<h6>Nearest Stations:<span>The distance between Mysore Junction and Brindavan Gardens is 21 km. The road distance is 21.1 km.</span></h6>
      </div>
      <div>
        <h2>Mysore palace</h2>
        <p>An incredibly breathtaking example of Indo - Saracenic style of architecture, the Mysore Palace is a magnificent edifice located in Mysore in the state of Karnataka. Also known as the Amba Vilas Palace, it is the former palace of the royal family of Mysore and is still their official residence. </p>
        <h6>Timings:<span>10:00 AM - 5:30 PM</span></h6>
        <h6>Nearest Stations:<span>At a distance of 2.5 km from the Mysore Railway Station & 2 km from KSRTC Bus Station.</span></h6>
      </div>
      <div>
        <h2>Chamundeswari temple</h2>
        <p>The Chamundeshwari Temple is a traditional Hindu temple located on the eastern edge of Mysore at the height of 1000ft on the Chamundi hills.</p>
        <h6>Timings:<span>7:30 AM - 2:00 PM, 3:30 PM - 6:00 PM, 7:30 PM - 9:00 PM</span></h6>
        <h6>Nearest Stations:<span>The Mysore Junction Railway Station is the nearest railway station at a distance of 13 kilometre from the temple.</span></h6>
      </div>
    </div>
  )
}
