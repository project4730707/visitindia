import React from 'react'

export default function Dehradun() {
  return (
    <div>
      <div>
        <h2>Forest Research Institute</h2>
        <p>Established in the year 1906, the Forest Research Institute is spread over 4.5 square kilometres and has an imposing Colonial and Greco-Roman styles of architecture. </p>
        <h6>Timings:<span>:00 AM - 5:30 PM</span></h6>
        <h6>Nearest Stations:<span>The nearest railway station to the Forest Research Institute Dehradun is the Dehradun Railway Station, located approximately 6 km away. </span></h6>
      </div>
      <div>
        <h2>Robbers's Cave</h2>
        <p>Robber's Cave, locally known as Gucchu Pani, is a river cave formed in the Himalayas, about 8 kilometres from Dehradun, Uttarakhand. </p>
        <h6>Timings:<span>7:00 AM - 6:00 PM</span></h6>
        <h6>Nearest Stations:<span>: Robber's Cave is situated at a distance of 8 km from the city of Dehradun. </span></h6>
      </div>
      <div>
        <h2>ShastraDhara</h2>
        <p>Sahastradhara literally means 'thousand-fold spring'. It is a popular attraction, famous for its medicinal & therapeutic value as its water contains sulphur.</p>
        <h6>Timings:<span>: 8:00 AM - 7:00 PM</span></h6>
        <h6>Nearest Stations:<span></span></h6>
      </div>
    </div>
  )
}
