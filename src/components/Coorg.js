import React from 'react'

export default function Coorg() {
  return (
    <div>
      <div>
        <h2>Golden Temple</h2>
        <p>Namdroling Monastery, situated at a distance of 34 km from the Coorg, is the largest teaching centre of the school of Tibetan Buddhism known as Nyingmapa. Popularly known as the 'Golden Temple'.</p>
        <h6>Timings:<span>: 9:00 AM - 6:00 PM</span></h6>
        <h6>Nearest Stations:<span>junction, 80 kms away is the nearest railway station.</span></h6>
      </div>
      <div>
        <h2>Raja's Seat</h2>
        <p>Raja's Seat is a garden built on an elevated ground overseeing the valleys that lie to the west, making it a popular viewpoint to watch sunrise and sunset.</p>
        <h6>Timings:<span>Garden: 5:30 AM - 7:30 PM</span></h6>
        <h6>Nearest Stations:<span>Hassan, Kannur, and Kasaragod are the nearby railway stations here.</span></h6>
      </div>
      <div>
        <h2>Talakaveri</h2>
        <p>Talakaveri is the source of the river Kaveri, located on the Brahmagiri hill (not to be confused with the Brahmagiri range further south) near Bhagamandala in Kodagu district, Karnataka.</p>
        <h6>Timings: <span>: 6:00 AM - 8:30 PM</span></h6>
        <h6>Nearest Stations:<span>nearest railway station is Mysore, Hassan, or Mangalore. </span></h6>
      </div>
      <div>
        <h2>Abbey Fallas</h2>
        <p>Abbey Falls, also known as Abbi Falls, is located around 10 kilometres away from the town of Madikeri and is one of the most popular tourist attractions in and around Coorg.</p>
        <h6>Timings:<span>9:00 AM - 5:00 PM, Daily</span></h6>
        <h6>Nearest Stations:<span>The nearest railway station to Abbey Falls is Mysore Railway stations. </span></h6>
      </div>
    </div>
  )
}
