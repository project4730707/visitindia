import React from 'react'
import './TripStyle.css'
import { Tripdata } from './Tripdata'
import Alleppey1 from '../assets/Alleppey/Houseboats/img1.webp'
import Alleppey2 from '../assets/Alleppey/Kuttanand/img1.jpg'
import Alleppey3 from '../assets/Alleppey/Light house/img1.jpg'

export const Alleppey = () => {
  return (
    <div className='trip'>
      <div className='tripcard'>
        <Tripdata 
         image={Alleppey1}
         Heading="House boats"
         text="A vacation to Kerala is incomplete without spending some relaxing time in the fantastic backwaters of Alleppey.Alleppey Houseboat experience allows guests to immerse themselves in the local culture. Cruise past traditional villages, witness daily life along the backwaters, and gain insights into the rich heritage of Kerala. "
         time="Best Timing to Visit : Sunrise to Sunset  Check-In: 12 PM | Check-Out: 9 AM"
         dis="Nearest Stations:Station, opp. KSRTC, Finishing Point, Alappuzha, Kerala 688013"
         />

<Tripdata 
         image={Alleppey2}
         Heading="Kuttanand"
         text="Kuttanad is a region covering a large part of Alappuzha and some of Kottayam district, the heart of the backwaters of Kerala. Kuttanad is the 'rice bowl of Kerala', being home to lush green rice fields spread extensively, divided by dykes."
         time="Best Timing to Visit: 12:00AM to 11:59PM"
         dis="Nearest Stations:Alappuzha railway station is the nearest to the region."
         />

<Tripdata 
         image={Alleppey3}
         Heading="Light House"
         text="Known for its breathtaking 360-degree views, the Alappuzha Lighthouse(Alleppey lighthouse) is an old-world charm."
         time="Best Timing to Visit: Tuesday - Sunday 9:00 AM - 11:45 AM 2:00 PM - 5:30 PM Closed on Mondays and between 11:45 AM-2:00 PM"
         dis=""
         />
         </div>
    </div>
  )
}
