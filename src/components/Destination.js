import React from 'react'
import Mahal1 from '../assets/Agra/TajMahal/img1.jpg'
import Mahal2 from '../assets/Agra/TajMahal/img3.jpg'
import Ujjain1 from '../assets/Ujjain/Ram mandir ghat/img1.jpg'
import Ujjain2 from '../assets/Ujjain/Temple/img1.jpg'
import './DestStyle.css'
import Destdata from './Destdata'

export const Destination = () => {
    return (
        <div className='destination'>
            <h1>Popular Destination</h1>
            <p>Tours give you the opportunity to see a lot,within a time frame </p>

            <Destdata
            className="firstdes"
                heading="Taj Mahal"
                text="The Taj Mahal is an ivory-white marble mausoleum on the right bank of the river Yamuna in Agra, Uttar Pradesh, India. It was commissioned in 1631 by the fifth Mughal emperor, Shah Jahan to house the tomb of his beloved wife, Mumtaz Mahal; it also houses the tomb of Shah Jahan himself."
                img1={Mahal1}
                img2={Mahal2}
            />
            <Destdata
            className="firstdesreverse"
                heading="Ujjain"
                text="Ujjain is an ancient city beside the Kshipra River in the central Indian state of Madhya Pradesh. An important Hindu pilgrimage destination, it’s known for the centuries-old Mahakaleshwar Temple, a towering structure with a distinctively ornate roof. Nearby, Bade Ganesh Temple houses a colorful statue of Ganesh, the elephant-headed Hindu deity. Harsiddhi Temple features a pair of tall dark pillars studded with lamps."
                img1={Ujjain1}
                img2={Ujjain2}
            />
        </div>
    )
}
