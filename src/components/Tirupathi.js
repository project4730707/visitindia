import React from 'react'

export default function Tirupathi() {
  return (
    <div>
      <div>
        <h2>Akasaganga</h2>
        <p>: Akasaganga Teertham is a waterfall in Tirupati, located at a distance of 3 km from the main temple. The waterfall has water flowing all throughout the year and holds immense religious significance.</p>
        <h6>Timings:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span>Akasa Ganga, the sacred waterfalls, is situated at a distance of 2 km from Tirumala Bus Station & 26 km from Tirupati Railway station</span></h6>
      </div>
      <div>
        <h2>Pushkarini lake</h2>
        <p>Almost neighboring the Sri Venkateshwara Temple, is Swami Pushkarini Lake. According to the legends, the lake belonged to Lord Vishnu and was located in Vaikuntham.</p>
        <h6>Timings:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span> Tiruchanur, Renigunta Junction, Tirupati West Halt, and Chandragiri railway station.</span></h6>
      </div>
      <div>
        <h2>Silathoranam</h2>
        <p>One of the natural marvels that history left behind, the Silathoranam, now, finds itself being an important landmark in archaeology and heritage.</p>
        <h6>Timings:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span> Tiruchanur, Renigunta Junction, Tirupati West Halt, and Chandragiri railway station.</span></h6>
      </div>
      <div>
        <h2>Sri venkateswara temple</h2>
        <p>Sri Venkateshwara Temple is the most revered and celebrated temple of Tirupati having tourists and pilgrims flooding in all year long. This is one of the holiest and the wealthiest temples in the world and millions of devotees flock here to pay their reverence</p>
        <h6>Timings:<span>September to October</span></h6>
        <h6>Nearest Stations:<span>Tirupati Main railway station (station code: TPTY) is a railway station located in the Indian state of Andhra Pradesh.</span></h6>
      </div>
    </div>
  )
}
