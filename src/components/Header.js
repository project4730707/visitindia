import React from 'react'
import { NavLink } from 'react-router-dom'
import { CiSearch } from "react-icons/ci";
import { RiHome7Fill } from "react-icons/ri";
import { AiFillExclamationCircle } from "react-icons/ai";
import { AiFillIdcard } from "react-icons/ai";







// import { FaBars } from "react-icons/fa";
// import { IoIosClose } from "react-icons/io";

export const Header = () => {
  // const  clicked =useClicked()
  // const handleClicked=()=>{
  //   clicked({
  //     close
  //   })
  // }
  return (
    <div>
        <header className='navlink'>
            <NavLink to='/' className='symbol'><h1>VISIT INDIA</h1></NavLink>
            {/* <button><FaBars   /></button>
            <button className='close'><IoIosClose/></button> */}
            <nav>
               <NavLink><input type='text' placeholder='Search' id='searchbar'  /><button id='searchicon'><CiSearch /></button></NavLink> 
                <NavLink to='/' className='link' ><RiHome7Fill  className='icon'/>Home</NavLink>
                <NavLink to='about' className='link'><AiFillExclamationCircle className='icon' />AboutUs</NavLink>
                <NavLink to='contact' className='link'><AiFillIdcard  className='icon'/>Contact</NavLink>
                <NavLink to='login' className='link' id='login'>Login</NavLink>
                {/* <NavLink  to="register"  className="link">Register</NavLink>  */}
            </nav>
        </header>
    </div>
  )
}
