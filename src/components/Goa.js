import React from 'react'

export default function Goa() {
  return (
    <div>
      <div>
        <h2>Calangute Beach</h2>
        <p>Calangute Beach is the longest beach in North Goa, stretching from Candolim to Baga. Due to its sheer size and popularity, it is a hub for tourists and backpackers from all over the world.</p>
        <h6>Timings:<span>After monsoon to enjoy the water sports</span></h6>
        <h6>Nearest Stations:<span>Thivim and Vasco Da Gama are the nearest railway station situated 38 kms away from Calangute beach.</span></h6>
      </div>
      <div>
        <h2>Cruise</h2>
        <p>Cruise in Goa allows one to explore the exotic scenery, pristine waters and marvel the breathtaking view of the sunset.</p>
        <h6>Timings: <span>Mandovi Sunset River Cruise: 5:30 PM to 6:00 PM. </span></h6>
        <h6>Nearest Stations:<span>Thivim and Vasco Da Gama are the nearest railway station situated 38 kms away from Calangute beach.</span></h6>
      </div>
      <div>
        <h2>Dudh sagar Waterfalls</h2>
        <p>One of India's tallest waterfalls, the Dudhsagar Falls is located inside the Mollem National Park. The majestic falls are a little farther inland approximately 60 km from Panaji on the Goa - Karnataka Border. </p>
        <h6>Timings:<span>Anytime</span></h6>
        <h6>Nearest Stations:<span>The nearest Station to the falls is Collem (Kuhlem) (South Central Railway). The route for rail to reach the falls from Goa is Collem – Mollem (6 km) – Dudhsagar Falls. 
 
 </span></h6>
      </div>
    </div>
  )
}
