import React from 'react'
import Coimbatore1 from '../assets/Coimbatore/Adiyogi shiva statue/img1.jpg'
import Coimbatore2 from '../assets/Coimbatore/Kovai kondattam/img2.jpg'
import Coimbatore3 from '../assets/Coimbatore/Sree ayyappan  temple/img1.jpg'
import Coimbatore4 from '../assets/Coimbatore/Vydehi falls/img1.jpg'

export const Coimbatore = () => {
  return (
    <div>
      <div>
      <img src={Coimbatore1} alt='..' />
        <h2>Adiyogi Shivs Statue</h2>
        <p> Adiyogi Shiva Statue is the world’s biggest bust statue dedicated to the famous Hindu deity Shiva, which is carved out of 500 tonnes of steel. </p>
        <h6>Timings: <span>6:00 AM - 8:00 PM</span></h6>
        <h6>Nearest Stations: <span>: The nearest railway station to Adiyogi Shiva Statue is Coimbatore Junction, which is around 23 kilometers away.</span></h6>
      </div>
      <div>
      <img src={Coimbatore2} alt='..' />
        <h2>Kovai kondattam</h2>
        <p>Kovai Kondattam is an amusement park located about 2 kilometres away from Perur, in Coimbatore, Tamil Nadu. A fun-filled destination ideal for spending hours of unending entertainment with friends and family, Kodai Kondattam is an eco-friendly theme park. </p>
        <h6>Timings: <span>Monday to Sunday: 10:30 AM to 5:30 PM</span></h6>
        <h6>Nearest Stations:<span>The nearest bus stand is the Perur Bus Stand, which is about 2 km from the park. The nearest railway station is the Coimbatore Junction, which is about 18 km from the park.</span></h6>
      </div>
      <div>
      <img src={Coimbatore3} alt='..' />
        <h2>Sree Ayyapan temple</h2>
        <p>Located in the centre of the city of Coimbatore in Tamil Nadu, India; the Sree Ayyappan Temple is a significant temple that is dedicated to Lord Sree Ayyappa. </p>
        <h6>Timings:5:00 AM - 11:00 AM & 5:00 PM - 9:00 PM</h6>
        <h6>Nearest Stations:<span>The nearest railway station is the Coimbatore Junction, which is about 18 km</span></h6>
      </div>
      <div>
      <img src={Coimbatore4} alt='..' />
        <h2>Vydehi falls</h2>
        <p>The Vydehi water falls is situated some 35 km away from the Coimbatore city. People who pay a visit to Coimbatore must make it a point to visit the Vydehi water falls, as it is one of the most familiar water fall and is highly known for its magnificence.</p>
        <h6>Timings: <span>24 hours</span></h6>
        <h6>Nearest Stations: <span>Nearest stations: Coimbatore Bus Stand, about 27 km away.Coimbatore Airport, about 38 km away.Coimbatore Railway Station about 37 km away.
</span></h6>
      </div>
    </div>
  )
}
