import React from 'react'

export const Ladakh = () => {
  return (
    <div>
      <div>
        <h2>Magnetic Hill</h2>
        <p>Nesting pretty at an altitude of 14000 feet above sea level, you have to take the Leh-Kargil-Baltic National Highway to reach the destination.</p>
        <h6>Timings:<span>July to September</span></h6>
        <h6>Nearest Stations:<span>The nearest railway station is Jammu Tawi Railway Station, which is around 700 kilometers away from Leh. From there, you can take a bus or hire a taxi to reach Magnetic Hill</span></h6>
      </div>
      <div>
        <h2>Nubra Valley</h2>
        <p>Nubra Valley lies in the union territory of Ladakh at a distance of around 140 Km from Leh. Located on the ancient Silk Route, the valley has Shyok and Nubra rivers snaking through it and some beautiful monasteries.</p>
        <h6>Timings:<span>June to September </span></h6>
        <h6>Nearest Stations:<span>Nubra can be reached by by bus or jeep from Leh, about 150 km away.</span></h6>
      </div>
      <div>
        <h2>Pongong Lake</h2>
        <p>The most popular tourist attraction in Ladakh, Pangong lake is an endorheic (landlocked) lake situated at 4350 meters. Also known as Pangong Tso it is 12 kilometres long and extends from India to Tibet</p>
        <h6>Timimgs:<span>It's open throughout the year however the best time is between 4:00 PM - 7:00 PM when the lake has it's popular colour.</span></h6>
        <h6>Nearest Stations:<span> Jammu is a railway station near it. The close by railway station is at Jammu, which is situated at a distance of 540 km.</span></h6>
      </div>
      <div>
        <h2>Shanti Gupta</h2>
        <p>The Shanti Stupa in Leh is a magnificent white-domed Buddhist monument located atop a steep hilltop at a dizzying height of 11,841 feet. It is a religious place for the Buddhists as it holds the relics of Buddha, consecrated by the 14th Dalai Lama.</p>
        <h6>Timings:<span> 5:00 AM - 9:00 PM Sunrise and Sunset</span></h6>
        <h6>Nearest Stations:<span> Jammu is a railway station near it</span></h6>
      </div>
    </div>
  )
}
