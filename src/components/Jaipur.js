import React from 'react'

export const Jaipur = () => {
  return (
    <div>
       <div>
        <h2>Amber fort</h2>
        <p>Cradled on the top of a hill near Jaipur lies the Amer Fort, one of the most magnificent palaces in India. Also commonly known as the Amber Fort,</p>
        <h6>Timings:<span>: 9:00 AM - 6:00 PM
Light Show timings: English: 7:30 PM, Hindi: 8:00 PM
</span></h6>
<h6>Nearest Stations:<span> take the RSRTC bus from Hawa Mahal in Jaipur to reach Amer Fort. This bus journey will take around 20 minutes to cover a distance of 7.2 kilometres and reach your final destination.

</span></h6>
      </div>
      <div>
        <h2>Birla temple</h2>
        <p>The magnificent Birla Mandir in Jaipur is a Hindu temple that forms a part of one of the several Birla temples located all around the country. Also known as the Lakshmi Narayan Temple, the shrine is situated on the Moti Dungari Hill. </p>
        <h6>Timings:<span>: 5:00 AM - 11:30 AM, 4:00 PM - 8:00 PM</span></h6>
        <h6>Nearest Stations:<span>metro station is Jhandewalan Metro Station, located only 3 km from Birla Mandir.</span></h6>

      </div>
      <div>
        <h2>City Palace</h2>
        <p>The magnificent City Palace in Jaipur is one of the most famous tourist attractions located in the old part of the city. Built by Maharaja Sawai Jai Singh during the years 1729 to 1732, the vast complex of the palace occupied one-seventh of the walled city.</p>
        <h6>Timings:<span>: 9:30 AM - 5:00 PM</span></h6>
        <h6>Nearest Stations:<span>The nearest metro station is the Chandpole Metro Station, which is about 1.9 kilometres away from the palace. </span></h6>
      </div>
      <div>
        <h2>Hawa Mahal</h2>
        <p>The massive edifice of Hawa Mahal stands at the intersection of the main road in Jaipur, Badi Chaupad and was built by Maharaja Sawai Pratap Singh in the year 1799.</p>
        <h6>Timings:<span>9:00 AM - 4:30 PM</span></h6>
        <h6>Nearest Stations:<span>Dahar Ka Balaji (DKBJ) Dist - JAIPUR (RAJASTHAN)</span></h6>
      </div>
    </div>
  )
}
