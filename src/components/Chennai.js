import React from 'react'
import Chennai1 from '../assets/Chennai/Marina beach/img1.jpg'
import Chennai2  from '../assets/Chennai/MGR film city/img1.jpg'
import Chennai3  from '../assets/Chennai/Temple/img2.jpg'

export const Chennai = () => {
  return (
    <div>
       <div>
       <img src={Chennai1} alt='..' />
        <h2>Marina Beach</h2>
        <p>Situated in the city of Chennai in Tamil Nadu, Marina Beach is a natural urban beach along the Bay of Bengal. </p>
        <h6>Timings:<span>Throughout the day</span></h6>
        <h6>Nearest Stations:<span>The nearest train stations to Marina Beach are Chepauk, Thiruvallikeni, and Lighthouse. Most of these stations are connected to Chennai Central railway station through local trains. It usually takes from 3 to 10 minutes to from Chennai Central to one of these stations.</span></h6>
      </div>
      <div>
      <img src={Chennai2} alt='..' />
        <h2>MGR film city</h2>
        <p>the MGR Film city is managed by the Government of Tamil Nadu in loving memory of MG Ramachandran who was not only a crowd-pleasing tamil actor but also a longtime CM of Tamil Nadu.</p>
        <h6>Timings: <span>. MGR film City remains open from 8 am to 8 pm .</span></h6>
        <h6>Nearest Stations: <span>Thiruvanmiyur Railway Station is 87 meters away, 2 min walk.</span></h6>
      </div>
      <div>
      <img src={Chennai3} alt='..' />
        <h2>Murudeswar temple</h2>
        <p>The magnificent Marundeeswarar Temple, in Tiruvanmiyur, near Chennai has the temple deity Shiva in the form of Marundeeswar or Aushadeeswarar, the God of Medicines.</p>
        <h6>Timings: <span>: 9:00 AM - 8:00 PM</span></h6>
        <h6>Nearest Stations: <span>: Thiruvanmiyur is 12 meters away, 1 min walk. Thiruvanmiyur Bus Depot is 310 meters away, 5 min walk. Thiruvanmiyur Depot is 325 meters away, 6 min walk.

</span></h6>
      </div>
    </div>
  )
}
