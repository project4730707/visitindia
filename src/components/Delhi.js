import React from 'react'

export const Delhi = () => {
  return (
    <div>
      <div>
        <h2>Hamayun's Tomb</h2>
        <p>Humayun's tomb is the final resting place of the Mughal Emperor Humayun. Located in the Nizamuddin East area of Delhi, it is the first garden-tomb in the Indian subcontinent. </p>
        <h6>Timings:<span>10:00 AM - 06:00 PM</span></h6>
        <h6>Nearest Stations:<span>Nearest Railway Station to Humayun's Tomb: Nizamuddin Railway Station at about 2.2 kilometres is the closest railway station to the Humayun's Tomb. </span></h6>
      </div>
      <div>
        <h2>Hauz Khas villages</h2>
        <p>Hauz Khas Village retains the old charm of the place with remnants of Islamic architecture roughly coloured by splotches of urbane refurbished upmarket.</p>
        <h6>Timings:<span>The Hauz Khas Village remains open from Monday to Saturday from 10:30 AM to 7:30 PM. The restaurant and pubs remain open till 1:00 AM in the night</span></h6>
        <h6>Nearest Stations:<span>: the nearest metro station is Green Park/Hauz Khas. From there, you can take an auto.</span></h6>
      </div>
      <div>
        <h2>India Gate</h2>
        <p>The All India War Memorial, popularly known as the India Gate, is located along the Rajpath in New delhi. This 42-meter tall historical structure was designed by Sir Edwin Lutyens and is one of the largest war memorials in the country. India Gate is also famous for hosting the Republic Day Parade every year. </p>
        <h6>Timings:<span>All time</span></h6>
        <h6>Nearest Stations:<span>The nearest train station to India Gate in Delhi is Pragati Maidan. </span></h6>
      </div>
      <div>
        <h2>Qutub minar</h2>
        <p>Qutub Minar is a minaret or a victory tower located in Qutub complex, a UNESCO World Heritage Site in Delhi's Mehrauli area. With the height of 72.5 metres (238 ft), Qutub Minar is the second tallest monument of Delhi.</p>
        <h6>Timings:<span>:00 AM - 5:00 PM. Qutub Minar remains open for tourists all days except Sunday</span></h6>
        <h6>Nearest Stations:<span>Qutub Minar Metro Sation is 208 meters away, 4 min walk.Qutab Minar Metro Station is 223 meters away, 4 min walk.Chhattarpur is 1383 meters away, 18 min walk.

</span></h6>
      </div>
    </div>
  )
}
