import React from 'react';



export const Hero = (props) => {
  return (
    <div className={props.cName}>
        <img alt='heroimg' src={props.heroImg}   />
       <div className='herotext'><h1>{props.title}</h1>
       <p>{props.text}</p> 
       <a href={props.url} className={props.btnClass}>{props.btnText}</a> </div> 
       
    
    </div>
  )
}
