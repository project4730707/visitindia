import React from 'react'

export const Hyderabad = () => {
  return (
    <div>
      <div>
        <h2>Charminar</h2>
        <p>An identifying feature of the city, Charminar is the most prominent landmark located right in the heart of Hyderabad.</p>
        <h6>Timimgs: <span>9:00 AM - 5:30 PM</span></h6>
        <h6>Nearest Stations:<span>The Charminar bus stop near Makkah Masjid on the Charminar Road in Panch Mohalla is the nearest bus stand to Charminar.</span></h6>
      </div>
      <div>
        <h2>Golconda fort</h2>
        <p>Situated in the Western part of the beautiful city of Hyderabad at a distance of approximately 9 km from Hussain Sagar Lake, Golconda Fort is one of the region's best-preserved monuments. </p>
        <h6>Timings:<span>8:00 AM to 5:30 PM</span></h6>
        <h6>Nearest Stations:<span>Nearest Railway Station :Hyderabad Deccan and Begampet Stations.Nearest Bus Station : Golconda Bus Station.
</span></h6>
      </div>
      <div>
        <h2>Hussain Sagar</h2>
        <p>Renowned as Asia's largest artificial lake, Hussain Sagar Lake is one of the most popular tourist attractions located in Hyderabad.</p>
        <h6>Timings:<span>8:00 AM - 10:00 PM Musical Floating Fountain Show Timings: On weekends (Saturday - Sunday) and public holidays - 4 shows (7:00 PM to 10:00 PM)</span></h6>
        <h6>Nearest Stations:<span>The nearest metro station is Lakdikapul Metro Station, located 17 minutes away.</span></h6>
      </div>
      <div>
        <h2>Ramoji film city</h2>
        <p>A place that is perfect for all the lovers of cinema and Bollywood! Set up by Ramoji Rao, the head of Ramoji group in 1991, Ramoji Film City is a spectacular getaway a little outside the city of Hyderabad. </p>
        <h6>Timings:<span>: 9:00 AM - 8:00 PM</span></h6>
        <h6>Nearest Stations:<span>The nearby railway station to Ramoji Film City are namely, Secunderabad Railway Station, Kacheguda Railway Station and Nampally Railway Station.</span></h6>
      </div>
      <div>
        <h2>Wonderla</h2>
        <p>Wonderla Amusement and Theme Park is the best water park in Hyderabad and one of the best things to do. With more than 43 fun and thrilling rides, it is a fun place for a family outing.</p>
        <h6>Timings:<span>: Amusement Park: 11:00 AM - 7:00 PM every day
Water Park: 12:00 PM - 6:00 PM every day
</span></h6>
<h6>Nearest Stations:<span>The nearest metro station to Wonderla is the Kengeri Metro Station, which is on the Purple Line.</span></h6>
      </div>
    </div>
  )
}
