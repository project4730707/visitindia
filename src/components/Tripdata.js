import React from 'react'
import "./TripStyle.css"



export const Tripdata = (props) => {
  
  return (
    <div className='tcards'>
      <div className='timg'> <img alt="image" src={props.image} /></div>
      <h2>{props.Heading}</h2>
      <h4>{props.heading}</h4>
        <p>{props.text}</p>
        <h5>{props.time}</h5>
        <h5>{props.dis}</h5>
        <a href={props.url} className={props.btnClass}>{props.btnText}</a> 
    </div>
  )
}
