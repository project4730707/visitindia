import React from 'react'
import './TripStyle.css'
import { Tripdata } from './Tripdata'
import Agra1 from '../assets/Agra/Agra fort/img1.jpg'
import Agra2 from '../assets/Agra/TajMahal/img1.jpg'
import Agra3 from '../assets/Agra/Fatehpur/img1.jpg'

export const Agra = () => {
  return (
    <div className='trip'>
      <div className='tripcard'>
        <Tripdata 
         image={Agra2}
         Heading="Taj maha"
         text="One of the seven wonders of the world, Taj Mahal is located on the banks of River Yamuna in Agra.Every Friday Taj Mahal is only accessible to practising Muslims to attend the prayers in the afternoon."
         time="Best Timing to Visit : Opens 30 minutes before sunrise and closes 30 minutes after sunset Closed on Friday"
         dis="Nearest Stations:Agra fort railway station,3km away"
         />

<Tripdata 
         image={Agra1}
         Heading="Agra Fort"
         text="An architectural masterpiece, The Red Fort of Agra or Agra Fort was built by Emperor Akbar in 1573. It is located on the right bank of the River Yamuna and is made entirely of red sandstone."
         time="Best Timing to Visit: In between  Sunrise to SunSet"
         dis="Nearest Stations:The nearest bus stop to Agra Fort Railway Station is the Idgah bus stand."
         />

<Tripdata 
         image={Agra3}
         Heading="Fatehpur"
         text="Fatehpur Sikri is a town in the Agra district and a famous tourist attraction. A city predominantly made of red sandstone, Fatehpur Sikri was founded in 1571 century by Mughal Emperor Akbar."
         time="Best Timing to Visit: Anytime"
         dis="Nearest Stations:Agra fort railway station"
         />
         </div>
    </div>
  )
}

