import React from 'react'

export default function Kodaikanal() {
  return (
    <div>
      <div>
        <h2>Bear Shola hills</h2>
        <p>the Bear Shola Falls is a popular picnic spot in the region. This cascade is a seasonal attraction that comes to life in its fullest during the monsoons. </p>
        <h6>Timings:<span>10:00 AM - 6:00 PM</span></h6>
        <h6>Nearest Stations:<span>Kodaikanal Bus Stand, about 2 km away.</span></h6>
      </div>
      <div>
        <h2>Green Valley</h2>
        <p>Formerly known as Suicide point, the Green Valley View offers a breathtaking view of the plains, deep valleys and hills. The mesmerizing view of the Vaigai Dam is an unforgettable experience. </p>
        <h6>Timings:<span>7:00 AM - 6:00 PM</span></h6>
        <h6>Neatrest Stations:<span>the closest railway station would be Kodai road which is a distance of 80 kms.</span></h6>
      </div>
      <div>
        <h2>Kodaikanal lake</h2>
        <p>Kodaikanal Lake is a manmade lake in the Kodaikanal city which is also known as Kodai Lake. This lake was developed by the British and early missionaries from the USASA.</p>
        <h6>Timings:<span>6:00 AM - 5:00 Pm</span></h6>
        <h6>Nearest Stations:<span>Kodaikanal bus stand is 3 kms away from the Kodai lake.</span></h6>
      </div>
      <div>
        <h2>Pillar Rocks</h2>
        <p>Situated in the 'Princess of Hill stations', Kodaikanal, the Pillar Rocks have become a lovely picnic spot. Constituting a beautiful mini garden; the place is named so as it has three vertically positioned boulders reaching up to a height of 400 feet.</p>
        <h6>Timings:<span>9:00 AM - 4:00 PM</span></h6>
        <h6>Nearest Stations:<span>Kodaikanal bus stand is the nearest bus stand to the pillar rocks. It is 7.5 km from the viewpoint. </span></h6>
      </div>
    </div>
  )
}
