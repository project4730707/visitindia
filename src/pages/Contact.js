import React from 'react'
import { Hero } from '../components/Hero'
import  Homeimg from '../assets/Munnar/Waterfalls/img2.jpg';

export const Contact = () => {
  return (
    <div>
    <Hero 
    cName="aboutpage"
    heroImg={Homeimg }
    title="Contact"
    />
    <div className='contact'>
    <section className="contact-box">
  <h2 className="contact-me">
    Contact <span>Me</span>
  </h2>
  <form action="#">
    <div className="input-box">
      <input type="text" placeholder="Full Name" />
      <input type="email" placeholder="Email" />
    </div>
    <div className="input-box">
      <input type="number" placeholder="Phone Number" />
      <input type="text" placeholder="Subject" />
    </div>
    <textarea name="" id="" cols={30} rows={10} defaultValue={"Your Message"} />
    <input type="submit" defaultValue="Send Message" className="btn1" />
  </form>
</section>

    </div>
  </div>
  )
}
