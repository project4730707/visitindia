import axios from "axios";
import React, { useState } from "react";
import {  useNavigate } from "react-router-dom";
const url = 'http://localhost:8001/user/register'

export const Register = () => {
    // const [empId, setEmpId] = useState("");
    const [Name, setName] = useState("");
    const [Gender, setGender] = useState("");
    const [Age, setAge] = useState("");
    const [MobileNumber, setMobileNumber] = useState("");
    const [Email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate()
  
    const handleSubmit = (e)=>{
      e.preventDefault();
      try{
          const data = {
              // empId,
              Name,
              Gender,
              Age,
              MobileNumber,
              Email,
              password,
          }
          const response = axios.post(url,data);
          console.log(response.data);
          clearFields();
          alert("Register Successfull")
          return navigate('/login')
  
      }catch(err){
          console.log(err);
          alert(" not Register Successfull")
      }
  }
  const clearFields =()=>{
      // setEmpId('');
      setName('')
      setGender('')
      setAge('')
      setMobileNumber('')
      setEmail('')
      setPassword('')
  }
  
    return (
      <div className="register">
        <h2>Register Here</h2>
        <form onSubmit={handleSubmit}>
          <fieldset>
            {/* <div>
              <label htmlFor="empid">EmpID</label>
              <input
                type="number"
                id="empid"
                name="empid"
                value={empId}
                onChange={(event) => {
                  setEmpId(event.target.value);
                }}
              />
            </div> */}
            <div>
              <label htmlFor="Name">Name :</label>
              <input
              className="textemail"
                type="text"
                id="Name"
                name="Name"
                value={Name}
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
            </div>
            <div>
            <label htmlFor="Gender">Gender :</label>
              <input
              className="textemail"
                type="text"
                id="Gender"
                name="Gender"
                value={Gender}
                onChange={(event) => {
                  setGender(event.target.value);
                }}
              />
            </div>
            
            <div>
            <label htmlFor="age">Age :</label>
              <input
              className="textemail"
                type="number"
                id="age"
                name="age"
                value={Age}
                onChange={(event) => {
                  setAge(event.target.value);
                }}
              />
            </div>
            <div>
            <label htmlFor="MobileNumber">MobileNumber :</label>
              <input
              className="textemail"
                type="number"
                id="MobileNumber"
                name="MobileNumber"
                value={MobileNumber}
                onChange={(event) => {
                  setMobileNumber(event.target.value);
                }}
              />
            </div>
            <div>
            <label htmlFor="Email">Email :</label>
              <input
              className="textemail"
                type="text"
                id="Email"
                name="Email"
                value={Email}
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
              />
            </div>
  
            <div>
            <label htmlFor="password">Password :</label>
              <input
              className="textemail"
                type="password"
                id="password"
                name="password"
                value={password}
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
              />
            </div>
            <div>
              <p>Already have an account?<a href='./Login' className="reg">Login</a></p>
              <button type="submit" className="login">Register</button>
            </div>
          </fieldset>
        </form>
      </div>
  )
}
