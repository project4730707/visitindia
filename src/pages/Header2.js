import React from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { CiSearch } from "react-icons/ci";
import { RiHome7Fill } from "react-icons/ri";
import { AiFillExclamationCircle } from "react-icons/ai";
import { AiFillIdcard } from "react-icons/ai";
import { useDispatch } from 'react-redux';

export const Header2 = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate()
 const handleSubmit=()=>{
    dispatch({
        type:"LOGOUT"
    })
    return navigate('/')
 }
  return (
    <div>
        <header className='navlink'>
        <NavLink to='/' className='symbol'><h1>VISIT INDIA</h1></NavLink>
            <nav>
            <NavLink><input type='text' placeholder='Search' id='searchbar'  /><button id='searchicon'><CiSearch /></button></NavLink> 
            <NavLink to='/' className='link' ><RiHome7Fill  className='icon'/>Home</NavLink>
            <NavLink to='about' className='link'><AiFillExclamationCircle className='icon' />AboutUs</NavLink>
            <NavLink to='contact' className='link'><AiFillIdcard  className='icon'/>Contact</NavLink>
            <NavLink to='places' className='link'>Places</NavLink>
            <button onClick={handleSubmit} className='logout'>Logout</button>
            </nav>
        </header>
    </div>
  )}