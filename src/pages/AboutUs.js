import React from 'react'
import { Hero } from '../components/Hero'
import Homeimg from '../assets/Goa/Beach/img2.jpg';


export const AboutUs = () => {
  return (
    <div>
      <Hero
        cName="aboutpage"
        heroImg={Homeimg}
        title="AboutUs"
      />
      <div className='aboutpar'>
        <h1>Our History</h1>
        <p>
          A "Visit India" webpage is a digital gateway to the enchanting wonders of this diverse and culturally rich nation. Embarking on a journey through the virtual corridors of this webpage offers a tantalizing glimpse into India's kaleidoscopic tapestry of experiences.

          At the forefront of this digital odyssey lies an immersive introduction to India, painting a vivid picture of its allure through captivating imagery and evocative descriptions. From the majestic Taj Mahal standing as a testament to eternal love to the bustling streets of Delhi pulsating with life, each pixelated frame beckons travelers to delve deeper into the Indian experience.

          Navigating through this digital terrain, visitors encounter a treasure trove of top destinations meticulously curated to showcase India's geographical and cultural diversity. Whether it's the serene backwaters of Kerala or the vibrant Pink City of Jaipur, each destination is a chapter waiting to be explored, enticing travelers with its unique charm and allure.

          Practicality meets passion as travelers find themselves equipped with indispensable travel essentials. Visa requirements, currency exchange tips, and transportation options seamlessly guide them on their journey, ensuring a smooth and hassle-free exploration of India's wonders.

          But beyond the logistics lies the heart and soul of India – its rich tapestry of culture and heritage. Vibrant festivals, traditional dances, and tantalizing cuisines beckon travelers to immerse themselves in the vibrant rhythm of Indian life, forging unforgettable memories and lifelong connections.

          For the adventurous spirit, India's vast landscapes offer boundless opportunities for exploration. From the snow-capped peaks of the Himalayas to the sun-kissed beaches of Goa, adrenaline-seekers find themselves amidst a playground of adventure and discovery.

          Yet amidst the exhilaration, serenity awaits in India's tranquil sanctuaries of wellness and spirituality. Yoga retreats, Ayurvedic spas, and meditation centers offer weary travelers a sanctuary for rejuvenation and self-discovery, amidst the chaos of everyday life.

          And as the digital journey draws to a close, travelers are left with a newfound appreciation for India's timeless allure. With testimonials from fellow explorers and avenues for further engagement, the "Visit India" webpage serves as a portal to endless possibilities, inviting travelers to embark on their own odyssey through the heart of this captivating nation.
        </p>

        <h2>Use of Visit India</h2>
        <p>
        A travel guide serves as an indispensable companion for adventurers venturing into the mesmerizing 
        landscape of India. Within the pages of this guidebook, travelers find themselves immersed in a
        world of discovery, where every turn of the page unveils a new wonder waiting to be explored.
        As travelers thumb through the meticulously curated sections, they are greeted with a wealth 
        of information designed to enrich their journey. From detailed descriptions of iconic landmarks 
        like the Taj Mahal and the ancient temples of Hampi to off-the-beaten-path gems hidden in the winding 
        alleys of Old Delhi, the travel guide is a treasure trove of insights, inviting travelers to delve deeper 
        into India's rich tapestry of culture and history.
        Practical advice leaps off the pages, offering invaluable tips on navigating India's vibrant streets, 
        from bargaining in bustling bazaars to deciphering the intricacies of the local transportation system. 
        Visa requirements, currency exchange rates, and essential phrases in regional languages ensure that travelers 
        are well-equipped to navigate the complexities of Indian travel with confidence and ease.
        </p>
      </div>

    </div>
  )
}
