import React from 'react'
import { Hero } from '../components/Hero'
import  Homeimg from '../assets/Delhi/Indiagate/img2.jpg';
import { Destination } from '../components/Destination';
import { Trip } from '../components/Trip';



export const Home = () => {
  return (
    <div>
      <Hero 
      cName="homepage"
      heroImg={Homeimg }
      title="Your Journey  Your Story"
      text="Choose Your Favourite Destination"
      url="login"
      btnText="Travel Plan"
      btnClass="show"
      />
      <Destination/>
      <Trip />
    </div>
  )
}
