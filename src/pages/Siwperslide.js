import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css'
import {Navigation, Pagination ,Scrollbar, Autoplay,EffectFade, EffectCube, EffectCards } from 'swiper/modules'
import Ladakh from '../assets/Ladakh/Magnetic hill/img2.jpg'
import Jammu  from '../assets/Jammu/Temple/img2.jpg'
import Kanyakumri from '../assets/Kanyakumari/Rock memorial/img2.jpg'
import Mysore from '../assets/Mysore/Gardens/img2.jpg'
import Agra1 from '../assets/Agra/Agra fort/img1.jpg'
import Alleppey from '../assets/Alleppey/Houseboats/img1.webp'
import Banglore from '../assets/Banglore/Lalbagh/img1.jpg'
import Chennai  from '../assets/Chennai/MGR film city/img1.jpg'
import Coimbatore from '../assets/Coimbatore/Kovai kondattam/img2.jpg'
import Delhi from '../assets/Delhi/Qutub minar/img2.jpg'
import Hyderabad from '../assets/Hyderabad/Charminar/img1.jpg'
import Jaipur from '../assets/Jaipur/Amber fort/img1.jpg'
const Swiperslide = () => {
    return (
        <div className='swiperslide'  >
            <Swiper 
            modules={[Navigation,Pagination,Scrollbar,Autoplay,EffectCube,EffectCards,EffectFade]}
            spaceBetween={50} 
            pagination={{clickable:true}}
            scrollbar={{draggable:true}}
            autoplay={{delay:3000}}
            // effect='cube'
            slidesPerView={1}
            loop={true}
            speed={3000}
           
            >
                <SwiperSlide><img src={Ladakh} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Jammu} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Kanyakumri} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Mysore} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Agra1} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Alleppey} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Hyderabad} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Chennai} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Coimbatore} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Jaipur} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Banglore} alt='..' /></SwiperSlide>
                <SwiperSlide><img src={Delhi} alt='..' /></SwiperSlide>
                
            </Swiper>
        </div>
    )
}

export default Swiperslide