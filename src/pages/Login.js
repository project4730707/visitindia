import axios from "axios"
import { useState } from "react";
import {  useNavigate } from "react-router-dom";
import {useDispatch}  from "react-redux";
const url = 'http://localhost:8001/user/login'

export const Login = () => {
  const [Email,setEmail] = useState('');
    const [password,setPassword] =useState('')
    const navigate = useNavigate();
    const dispatch=useDispatch()

    const handleSubmit = async (e)=>{
        e.preventDefault();
      
        try{
            const response = await axios.post(url, {
                Email: Email,
                password: password
            });

            dispatch({
              type:"LOGIN",
            })
            
            if(response.status === 200){
                alert("Login Success");
                clearFileds();
                return navigate('/')
            }
        }catch(err){
          alert(" Incorrect Email or password")
            console.log(err);
            clearFileds()
        }
    }
    const clearFileds = ()=>{
        setEmail('')
        setPassword('')
    }
  return (
    <div className="container">
      <h2>Login Here</h2>
        <form onSubmit={handleSubmit}>
        <fieldset>
          <div className="emailbox">
            <label htmlFor="Email">UsernameorEmail :</label>
            <input
            className="textemail"
              type="Email"
              id="Email"
              name="Email"
              placeholder="Username or Email"
              value={Email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div className="passwordbox">
            <label htmlFor="password">Password :</label>
            <input
            className="textemail"
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <p>Don't Have an Account/<a href='./Register' className="reg">Register</a></p>

          <div>
            <button   type="submit" className="login">Login</button>
        </div>
        </fieldset>
      </form>
    </div>
  )
}
