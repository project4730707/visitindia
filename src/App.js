
import { Route, Routes } from 'react-router-dom';
import './App.css';
import { Footer } from './components/Footer';
import { Header } from './components/Header';
import { Home } from './pages/Home';
import { AboutUs } from './pages/AboutUs';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
// import Logout from './pages/Logout';
import { useSelector } from 'react-redux';
import { Header2 } from './pages/Header2';
import { Contact } from './pages/Contact';
import { Places } from './components/Places';
import { Agra } from './components/Agra';
import { Alleppey } from './components/Alleppey';
import { Banglore } from './components/Banglore ';
import { Chennai } from './components/Chennai';
import {Coimbatore } from './components/Coimbatore';
import { Delhi } from './components/Delhi';
import { Hyderabad } from './components/Hyderabad';
import { Jaipur  } from './components/Jaipur';
import { Jammu } from './components/Jammu';
import { Kanyakumari } from './components/Kanyakumari';
import { Ladakh  } from './components/Ladakh';
import { Mysore } from './components/Mysore';

// import { Placesrouter } from './components/Placesrouter';




function App() {
  const login = useSelector((state) => state.login)
  return (
    <div className="App">
      {login ? <Header2 />:<Header />}
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='about' element={<AboutUs />} />
        <Route path='contact' element={<Contact />} />
        <Route path='login' element={<Login />} />
        <Route path='register' element={<Register />} />
        <Route path='places' element={<Places />} />
        <Route path='agra' element={<Agra />} />
        <Route path='alleppey' element={<Alleppey />} />
        <Route path='banglore' element={<Banglore />} />
        <Route path='chennai' element={<Chennai />} />
        <Route path='coimbatore' element={<Coimbatore />} />
        <Route path='delhi' element={<Delhi />} />
        <Route path='hyderabad' element={<Hyderabad />} />
        <Route path='jaipur' element={<Jaipur />} />
        <Route path='jammu' element={<Jammu />} />
        <Route path='kanyakumari' element={<Kanyakumari />} />
        {/* <Route path='kodaikanal' element={<Kaodaikanal />} /> */}
        <Route path='ladakh' element={<Ladakh />} />
        <Route path='mysore' element={<Mysore />} />
        {/* <Route path='places' element={<Placesrouter />} /> */}
        
        
      </Routes>


      
      <Footer />
    </div>
  );
}

export default App;
